#include "Pion.h"
#include "console.h"

#include <iostream>

#define XORIGIN 6
#define YORIGIN 3
#define XINCR 4
#define YINCR 2

//constructeur par d�faut
Pion:: Pion()
{
    m_i = 0;
    m_j = 0;
    m_player = false;
    visited = false;
}

//constructeur surcharg�
Pion:: Pion(int i, int j, bool play)
        : m_i(i),m_j(j),m_player(play),visited(true)
{

}

Pion:: ~Pion()
{

}

///getters

int Pion:: getLig()
{
    return m_i;
}
int Pion:: getCol()
{
    return m_j;
}
bool Pion:: getPlayer()
{
    return m_player;
}
bool Pion:: getVisit()
{
    return visited;
}
///setters

void Pion:: setLig(int _i)
{
    m_i = _i;
}
void Pion:: setCol(int _j)
{
    m_j = _j;
}

void Pion:: setPlayer(bool _play)
{
    m_player = _play;
}

///m�thodes

void Pion:: render()
{
     // pointeur pour utiliser les fct de gotoligcol
    Console* pConsole = NULL;
    //allocation de memoire pour le ponteur
    pConsole = Console:: getInstance();

    if(this->visited)
    {
        pConsole->gotoLigCol(this->m_i*YINCR +YORIGIN ,this->m_j*XINCR + XORIGIN );

        if(this-> m_player)
        {
            pConsole->setColor(COLOR_RED);
            std:: cout<< "\xdb";
        }else{
         pConsole->setColor(COLOR_GREEN);
         std:: cout<< "\xdb";

    }
    pConsole->setColor(COLOR_WHITE);
    pConsole->gotoLigCol(this->m_i*YINCR +YORIGIN ,this->m_j*XINCR + XORIGIN);

    }

    pConsole->deleteInstance();


}

