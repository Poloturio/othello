#include "Node.h"
#include "Pion.h"
#include "Fonction.h"
#include "console.h"
#include <vector>
#include <utility>
#include <cstdlib>
#include <ctime>
#include <iostream>

#define NBCELLS 8
#define PLAY1 0
#define PLAY2 1
#define MINLIG 3
#define MINCOL 6
#define XINCR 4
#define YINCR 2
#define INFINITE 9000
#define ERASELINES 10
#define NINETYNINE 99
#define TWENTYFOUR 24
#define EIGHT 8
#define SEVEN 7
#define SIX 6
#define FOUR 4
#define THREE 0

Node:: Node()
{
    ///constructeur par d�faut qui dimension le double vecteur et qui appele le constructeur de pion
    board.resize( NBCELLS ,std:: vector<Pion>( NBCELLS , Pion() ) );
    eval = 0;
    player =false;
    coup = std:: pair<int,int> (0,0);
}
///constructeur surcharg� qui prend en copie un tableau
Node:: Node(std:: vector<std::vector<Pion> > _board ,std:: pair <int, int> _coup)
        : board(_board),coup(_coup)
{
    player =false;

}
Node :: Node(std:: vector < std:: vector <Pion> > _board)
        :board(_board)
{
    eval = 0;
    player =false;
    coup = std:: pair<int,int> (0,0);

}
//destructeur
Node:: ~Node()
{
}
///setter pour changer le joeur qui joue sur le tableau
void Node:: setPlayer(bool play)
{
    player = play ;
}
///initialisation du tableau si on joue une nouvelle partie
void Node:: boardInit()
{
        ///intitialisation du tableau
    board[3][3] =  Pion(3,3,PLAY1);
    board[4][4] =  Pion(4,4,PLAY1);
    board[3][4] =  Pion(3,4,PLAY2);
    board[4][3] =  Pion(4,3,PLAY2);
}
///dessine tous les pions sur le tableau
void Node:: drawBoard(bool play)
{
    for(int i = 0; i< NBCELLS ; i++)
    {
        for(int j = 0; j< NBCELLS ; j++)
        {
            ///appele la m�thode pion qui va le dessiner sur la console
            board[i][j].render();
        }
    }
    score(play,*this);
}

///getter
int Node:: getMove(int coor)
{
    if(coor == 1)
    {
        return coup.first;
    }else return coup.second;
}


void Node:: poserPion(int i,int j, bool &play)
{
    //declaration des variables
    bool valid;
    ///remise des coord�es console en coordon�es relative sur le tableau 8X8


    ///test affiche des coordonn�es
    //std::cout <<i << std::endl<<j;

    ///partie superposition
    //si il n'y a pas de pion alors on peut le poser si il encadre un pion enemi
    if( board[i][j].getVisit() == false)
    {
       // std::cout << "not superposed";
       for (int n = 0; n < NBCELLS ; n++)
       {
           int depth = 0;

           if(checkDir(i,j,depth,play,n))
           {
               valid = true;
           }

        }
        if(valid == true)
        {
               board[i][j] = Pion(i,j,play);
               /// affichage des noueveau pions


            play = !play;
            player = play;
            //score(play,*this);
        }


    }
}
bool Node:: checkDir(int i, int j,int depth,bool play, int dir)
{
    bool possible = false;
    //tableau qui donnent les changements en XY pour les 8 d�placements
    ///dans l'ordre: haut, haut droite, droite, bas droite, bas, bas gauche, gauche, haut gauche
    int dirLig [] = {-1,-1,0,1,1,1,0,-1};
    int dirCol [] = {0,1,1,1,0,-1,-1,-1};
    ///mise � jours des variables pour se deplacer
    i += dirLig[dir];
    j += dirCol[dir];
    ///controle des bords
    if(i >= 0 && i < 8 && j >= 0 && j < 8)
    {
        ///premier cas
        if(board[i][j].getVisit() == false)
        {
            ///on est tomb� sur une case vide on renvoit faux
            ///la direction n'est pas valide
            return false;
        }
        if(board[i][j].getPlayer() != play)
        {
            ///alors on tombe sur un pion adverse, on incr�ment le compteur de profondeur et on appelle la fonction une deuxi�me fois
            ///partie r�cursive
            depth+=1;
            possible = checkDir(i,j,depth,play,dir);
            if (possible)
            {
                board[i][j].setPlayer(play);
                return true;
            }
        }
        if(board[i][j].getPlayer() == play && depth >= 1)
        {
            /// on est tomb� sur un pion ami avec une distance sup � 1
            // la direction est valide, on renvoit vrai
             return true;
        }

    }else return false;
    return possible;
}
bool Node:: correctDir(int i, int j, int depth, bool play,int dir)
{
    bool possible = false;
    //tableau qui donnent les changements en XY pour les 8 d�placements
    ///dans l'ordre: haut, haut droite, droite, bas droite, bas, bas gauche, gauche, haut gauche
    int dirLig [] = {-1,-1,0,1,1,1,0,-1};
    int dirCol [] = {0,1,1,1,0,-1,-1,-1};
    ///mise � jours des variables pour se deplacer
    i += dirLig[dir];
    j += dirCol[dir];
    ///controle des bords
    if(i >= 0 && i < 8 && j >= 0 && j < 8)
    {
        ///premier cas
        if(board[i][j].getVisit() == false)
        {
            ///on est tomb� sur une case vide on renvoit faux
            ///la direction n'est pas valide
            return false;
        }
        if(board[i][j].getPlayer() != play)
        {
            ///alors on tombe sur un pion adverse, on incr�ment le compteur de profondeur et on appelle la fonction une deuxi�me fois
            ///partie r�cursive
            depth+=1;
            possible = correctDir(i,j,depth,play,dir);
            if (possible)
            {
                //board[i][j].setPlayer(play);
                return true;
            }
        }
        if(board[i][j].getPlayer() == play && depth >= 1)
        {
            /// on est tomb� sur un pion ami avec une distance sup � 1
            // la direction est valide, on renvoit vrai
             return true;
        }

    }else return false;
    return possible;
}

///fonction pour que une IA bete puisse jouer
/// va regarder tout les coups posssibles � jouer e en choisir un au hasard
void Node:: AIMove(bool &play)
{
    std:: srand(time(NULL));


    //tableau 2d pour stocker tout les coups possibles
    std:: vector <int> moveLig;
    std:: vector <int> moveCol;

    ///on boucle a travers toutes les cellules
    for(int i = 0; i < NBCELLS; i++)
    {
        for(int j =0; j<NBCELLS; j++)
        {
            ///si la case est vide
            if(board[i][j].getVisit() != true)
            {
                bool possible = false;
                /// on v�rifie dans toutes les directions
                for(int dir = 0; dir < NBCELLS ; dir++)
                {
                    int depth = 0;
                    if(correctDir(i,j,depth,play,dir) == true)
                    {
                        possible = true;

                    }

                }
                if(possible)
                {
                    ///si le coup est valide, on ajoute ses coord dans le tableau des coups posibles
                    moveLig.push_back(i);
                    moveCol.push_back(j);
                }
            }
        }
    }
///si il y a au moins un coup possible alors on peut jouer
    if(moveCol.size() > 0)
    {
        ///on choisit un index au hasard parmi tout les coups possibles
        int index = rand()%moveCol.size();

        displayMoves(moveLig,moveCol,index);
        poserPion(moveLig[index],moveCol[index],play);
        drawBoard(play);

    }else play = !play;


}
std:: pair<int,int> Node:: CountTiles()
{
    ///compte le nombre de case et les pions � l'interieur pour afficher le score
    //boucle � travers tout le tableau
    int J1 = 0;
    int J2 = 0;

    for(unsigned int i=0; i<NBCELLS;i++)
    {
        for(unsigned int j =0; j< NBCELLS;j++)
        {
            if(board[i][j].getVisit() == true)
            {
                if(board[i][j].getPlayer()== true)
                {
                    ///incr�mentation du score
                    J1++;
                }else J2++;
            }
        }
    }
    ///on retourne le score sous forme de paire
    std:: pair<int,int> J12 (J1,J2);
    return J12;
}
bool Node:: checkEnd(bool play)
{
    ///on boucle a travers toutes les cellules
    for(int i = 0; i < NBCELLS; i++)
    {
        for(int j =0; j<NBCELLS; j++)
        {
            ///si la case est vide
            if(board[i][j].getVisit() != true)
            {
                bool possible = false;
                /// on v�rifie dans toutes les directions
                for(int dir = 0; dir < NBCELLS ; dir++)
                {
                    int depth = 0;
                    if(correctDir(i,j,depth,play,dir) == true)
                    {
                        possible = true;
                        break;
                    }

                }
                if(possible)
                {
                    ///si au moins une seule case est valide alors on peut sortir de la boucle
                    return true;
                }
            }
        }
    }

    return false;
}
 int Node:: MiniMax(int depth, bool play, int counter, bool possibility,int i, int j)
 {
    std:: srand(time(NULL));
    // counter++;
    //std:: cout << counter << " " << std:: endl;
     /// cas de fin de fonction r�cursive pour ne pas boucler � l'infini
    if(depth == 0 || !checkEnd(play))
    {
        ///return heuristic value of the node
      // std::cout << "in";
        return EvalBoard(play);
    }
    std:: vector < std:: pair <int,int> > moves;
    //std::cout << depth;
    if(!play)
    {
        /// intitialisation de la valeur max
        int maxi = -1*INFINITE;

        moves = PlayMoves(play);
        //std::cout << moves.size();

        ///alors on est le joueur qui maximize
        ///on boucle sur tout les coups possibles
        for(unsigned int n = 0; n < moves.size(); n++)
        {
            counter++;
            Node b = Node(board,std:: pair <int,int> (moves[n].first,moves[n].second));
            b.poserPion(b.coup.first,b.coup.second,play);
            b.eval = b.MiniMax(depth-1,!play,counter,false,i,j);
            if(b.eval >= maxi)
            {
                if( b.eval == maxi )
                {
                    if(rand()%2 == 1)
                    {
                        maxi = b.eval;
                    coup = std:: pair <int,int> (moves[n].first,moves[n].second);

                    }


                }else {
                    maxi = b.eval;
                    coup = std:: pair <int,int> (moves[n].first,moves[n].second);

                }


            }
            if(possibility){
                showPossibilities(i,j,b.eval,moves[n].first,moves[n].second);
            }
        }
        if(possibility){
                showChoice(i,j,maxi,coup);
            }
        return maxi;


    }else{
        ///sinon on est le joueur qui minimise
        ///initialisation de la valeur min
        int mini = INFINITE;

        moves = PlayMoves(play);
        //std::cout << moves.size();
        ///on boucle sur tout les coups possibles
         for(unsigned int n = 0; n < moves.size(); n++)
        {
            counter++;
            Node b = Node(board,std:: pair <int,int> (moves[n].first,moves[n].second));
            b.poserPion(coup.first,coup.second,play);
            b.eval = b.MiniMax(depth-1,!play,counter,false,i,j);
            if(b.eval <= mini)
            {
                 if( b.eval == mini )
                {
                    if( rand()%2 == 1)
                    {
                        mini = b.eval;
                    coup = std:: pair <int,int> (moves[n].first,moves[n].second);

                    }


                }else {
                    mini = b.eval;
                    coup = std:: pair <int,int> (moves[n].first,moves[n].second);

                }

            }
        }
        return mini;
    }
 }
int Node:: EvalBoard(bool play)
 {
    int evaluation= 0;

     ///table de poids pour evaluer dans un premier temps le tableau

     int weight [NBCELLS*NBCELLS] = {  NINETYNINE, -EIGHT, EIGHT,  SIX,  SIX,  EIGHT,  -EIGHT,NINETYNINE,
                                       -EIGHT, -TWENTYFOUR,  -FOUR, -THREE, -THREE, -FOUR, -TWENTYFOUR,  -EIGHT,
                                        EIGHT, -FOUR, SEVEN,  FOUR,  FOUR,  SEVEN,  -FOUR, EIGHT,
                                        SIX  -THREE, FOUR,  PLAY1,  PLAY1,  FOUR,  -THREE, SIX,
                                        SIX,  -THREE, FOUR,  PLAY1,  PLAY1,  FOUR,  -THREE, SIX,
                                        EIGHT,  -FOUR, SEVEN,  FOUR,  FOUR,  SEVEN,  -FOUR, EIGHT,
                                        -EIGHT, -TWENTYFOUR,  -FOUR, -THREE, -THREE, -FOUR, -TWENTYFOUR,  -EIGHT,
                                        NINETYNINE, -EIGHT, EIGHT,  SIX,  SIX,  EIGHT,  -EIGHT, NINETYNINE };
    ///premiere �tape de l'evaluation avec la matrice

    for(int i = 0; i< NBCELLS ;i++)
    {
        for(int j = 0; j< NBCELLS ; j++)
        {
            if(board[i][j].getVisit() )
            {
                if(board[i][j].getPlayer() == play)
                {
                    evaluation += weight[i* NBCELLS + j];
                }else evaluation -= weight[i* NBCELLS + j];

            }
        }
    }

    std:: vector < std:: pair <int,int> > moves;
    moves = PlayMoves(!play);

    evaluation -= moves.size()* 5 ;

    if(moves.size() == 0)
    {
     //evaluation = INFINITE;
    }


    return evaluation;


 }

 int Node:: checkState(int i, int j)
 {
     if (board[i][j].getVisit()==true && board[i][j].getPlayer() ==true)
     {
         return 1;
     }
     if (board[i][j].getVisit()==true && board[i][j].getPlayer() ==false)
     {
         return 2;
     }
     else
     {
         return 0;
     }
 }
 ///
 ///methode pour simuler tous les coups possibles sur un plateau
 //
 std::vector<std:: pair<int,int>> Node:: PlayMoves(bool play)
 {
     std:: vector < std:: pair <int,int> > moves;
     ///on boucle � travers tout le tableau
     for(int i = 0;i< NBCELLS;i++)
     {
         for(int j = 0;j < NBCELLS ; j++)
         {
             ///si la case est vide
             if( board[i][j].getVisit() == false)
             {



                /// on v�rifie dans toutes les directions
                for(int dir = 0; dir < NBCELLS ; dir++)
                {
                    int depth = 0;
                    if(correctDir(i,j,depth,play,dir) == true)
                    {
                        moves.push_back(std:: pair <int,int> (i,j) );
                        break;

                    }

                }
                /*if(possible)
                {
                    ///si le coup est valide, on ajoute ses coord dans le tableau des coups posibles

                }
                */

             }
         }
     }
     return moves;
 }
  void Node:: showPossibilities(int &i,int &j,int worth, int coupLig,int coupCol)
  {
      char charCol ;
       // pointeur pour utiliser les fct de getch()
    Console* pConsole = NULL;
    //allocation de memoire pour le ponteur
    pConsole = Console:: getInstance();

    pConsole->gotoLigCol(i,j);

    switch (coupCol)
        {
            case 0:
                charCol = 'A';
            break;
            case 1:
                charCol = 'B';
            break;
            case 2:
                charCol = 'C';
            break;
            case 3:
                charCol = 'D';
            break;
            case 4:
                charCol = 'E';
            break;
            case 5:
                charCol = 'F';
            break;
            case 6:
                charCol = 'G';
            break;
            case 7:
                charCol = 'H';
                break;
            default :
                charCol = 'X';

        }

    std:: cout << coupLig +1 << " " << charCol << " : val = " << worth << "    " ;

    for(int k =1;k<ERASELINES;k++)
    {
        pConsole->gotoLigCol(i+k,j);
        std::cout << "                              ";


    }

    i += 1;

  }
  void Node:: showChoice(int i,int j,int maxi,std::pair<int,int> coup)
  {
      char charCol;
       // pointeur pour utiliser les fct de getch()
    Console* pConsole = NULL;
    //allocation de memoire pour le ponteur
    pConsole = Console:: getInstance();

    pConsole->gotoLigCol(i,j);

    pConsole ->setColor(COLOR_GREEN);

    switch (coup.second)
        {
            case 0:
                charCol = 'A';
            break;
            case 1:
                charCol = 'B';
            break;
            case 2:
                charCol = 'C';
            break;
            case 3:
                charCol = 'D';
            break;
            case 4:
                charCol = 'E';
            break;
            case 5:
                charCol = 'F';
            break;
            case 6:
                charCol = 'G';
            break;
            case 7:
                charCol = 'H';
                break;
            default :
                charCol = 'X';

        }

    std::cout << "valeur maxi: " << maxi << " , coup : " << coup.first + 1 << " " << charCol ;
    pConsole ->setColor(COLOR_WHITE);

  }
