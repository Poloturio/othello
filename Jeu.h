#ifndef JEU_H_INCLUDED
#define JEU_H_INCLUDED

#include <vector>
#include "Node.h"
#include "Pion.h"

void Menu();
void Game(bool play,Node Board);
void SaveGame(Node b, bool play);
Node loadGame(bool &play);
bool EndGame(Node b, bool &play);


#endif // JEU_H_INCLUDED
