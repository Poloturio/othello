#ifndef NODE_H_INCLUDED
#define NODE_H_INCLUDED

#include <vector>
#include <utility>

#include "Pion.h"

class Pion;

class Node
{
private:
        std:: vector < std:: vector <Pion> > board;
        std:: pair <int,int> coup;
        bool player ;
        int eval;
public:
    Node();
    Node(std:: vector < std:: vector <Pion> > _board, std:: pair <int,int> _coup);
    Node(std:: vector < std:: vector <Pion> > _board);
    ~Node();
    ///setter
    void setPlayer(bool play);
    ///getter
    int getMove(int coord);
    ///m�thodes
    void IAMove(std:: vector < std:: vector <Pion> > &b , bool &play);
    void boardInit();
    void drawBoard(bool play);
    void poserPion(int i,int j, bool &play);
    bool checkDir(int i, int j,int depth,bool play, int dir);
    bool correctDir(int i, int j, int depth, bool play,int dir);
    void AIMove(bool &play);
    std:: pair<int,int> CountTiles();
    bool checkEnd(bool play);
    int MiniMax(int depth, bool play, int counter, bool possibility,int i,int j);
    int EvalBoard(bool play);
    int checkState(int i, int j);
    std::vector<std:: pair<int,int>> PlayMoves(bool play);
    void showPossibilities(int &i,int &j,int eval,int coupLig,int coupCol);
    void showChoice(int i,int j,int maxi,std::pair<int,int> coup);

};


#endif // NODE_H_INCLUDED
