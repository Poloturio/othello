#ifndef PION_H_INCLUDED
#define PION_H_INCLUDED

#include <vector>
#include "Node.h"


class Pion
{
    private:
    ///attributs
    int m_i,m_j;
    bool m_player;
    bool visited;

    public:
        ///construteuc surchargé et destructeur
        Pion();
        Pion(int i,int j,bool play);
        ~Pion();

        ///getter
        int getLig();
        int getCol();
        bool getPlayer();
        bool getVisit();
        ///setters
        void setLig(int _i);
        void setCol(int _j);
        void setPlayer(bool _play);
        ///methodes
        void render();

};


#endif // PION_H_INCLUDED
