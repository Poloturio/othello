#include "Fonction.h"
#include "console.h"
#include "Pion.h"
#include "Node.h"
#include "Jeu.h"

#include <string>
#include <iostream>
#include <vector>
#include <utility>
#include <cmath>

#define MAXLIG 17
#define MAXCOL 34
#define MINLIG 3
#define MINCOL 6
#define XINCR 4
#define YINCR 2
#define NBCELLS 8
#define ERASELINES 15


using namespace std;

///impression du tableau de jeu
void tabprint()
{
    // pointeur pour utiliser les fct de gotoligcol
    Console* pConsole = NULL;
    //allocation de memoire pour le ponteur
    pConsole = Console:: getInstance();
    int i = MINLIG - 1;
    int col = MINCOL - 2;
//    string test = {192,196,238,' ','e' };
    string str1 = "\xda\xc4\xc4\xc4\xc2\xc4\xc4\xc4\xc2\xc4\xc4\xc4\xc2\xc4\xc4\xc4\xc2\xc4\xc4\xc4\xc2\xc4\xc4\xc4\xc2\xc4\xc4\xc4\xc2\xc4\xc4\xc4\xbf";
    string str2 = "\xb3   \xb3   \xb3   \xb3   \xb3   \xb3   \xb3   \xb3   \xb3";
    string str3 = "\xc3\xc4\xc4\xc4\xc5\xc4\xc4\xc4\xc5\xc4\xc4\xc4\xc5\xc4\xc4\xc4\xc5\xc4\xc4\xc4\xc5\xc4\xc4\xc4\xc5\xc4\xc4\xc4\xc5\xc4\xc4\xc4\xb4";
    string str4 = "\xc0\xc4\xc4\xc4\xc1\xc4\xc4\xc4\xc1\xc4\xc4\xc4\xc1\xc4\xc4\xc4\xc1\xc4\xc4\xc4\xc1\xc4\xc4\xc4\xc1\xc4\xc4\xc4\xc1\xc4\xc4\xc4\xd9";

    //affichage du tableau sur la console
    pConsole->gotoLigCol(i-1,col+2);
    cout<<"A   B   C   D   E   F   G   H";
     pConsole->gotoLigCol(i, col);
    cout << str1;
    i++;
    pConsole->gotoLigCol(i, col-1);
    cout <<"1"<< str2;
    i++;
    pConsole->gotoLigCol(i, col);
    cout << str3;
    i++;
    pConsole->gotoLigCol(i, col-1);
    cout <<"2"<< str2;
    i++;
    pConsole->gotoLigCol(i, col);
    cout << str3;
    i++;
    pConsole->gotoLigCol(i, col-1);
    cout <<"3"<< str2;
    i++;
    pConsole->gotoLigCol(i, col);
    cout << str3;
    i++;
    pConsole->gotoLigCol(i, col-1);
    cout <<"4"<< str2;
    i++;
    pConsole->gotoLigCol(i, col);
    cout << str3;
    i++;
    pConsole->gotoLigCol(i, col-1);
    cout <<"5"<< str2;
    i++;
    pConsole->gotoLigCol(i, col);
    cout << str3;
    i++;
    pConsole->gotoLigCol(i, col-1);
    cout <<"6"<< str2;
    i++;
    pConsole->gotoLigCol(i, col);
    cout << str3;
    i++;
    pConsole->gotoLigCol(i, col-1);
    cout <<"7"<< str2;
    i++;
    pConsole->gotoLigCol(i, col);
    cout << str3;
    i++;
    pConsole->gotoLigCol(i, col-1);
    cout <<"8"<< str2;
    i++;
    pConsole->gotoLigCol(i, col);
    cout << str4;

    pConsole->gotoLigCol(3,6);
    //desalocation du pointeur sur la classe console
    Console::deleteInstance();

}
 //brief: saisie de la direction par le joueur et de la vvalidation d'un coup
 //
 //param  i indice ligne
 //param  j indice colone
 //return  rien
 //
 //


bool saisieDir(int &i,int &j, Node &b , bool &play)
{
    // pointeur pour utiliser les fct de getch()
    Console* pConsole = NULL;
    //allocation de memoire pour le ponteur
    pConsole = Console:: getInstance();

    char dir;

    dir = pConsole->getInputKey();

    ///direction vers le haut
    if ( dir == 'z' )
    {
        if( i != MINLIG)
        {
            i-= YINCR;
            pConsole->gotoLigCol(i,j);
        }else {
                i = MAXLIG;
                pConsole->gotoLigCol(i,j);
        }
    }
    ///direction vers la gauche
    if ( dir == 'q' )
    {
        if( j != MINCOL)
        {
            j-= XINCR;
            pConsole->gotoLigCol(i,j);
        }else {
            ///d�bordement du terrain
                j = MAXCOL;
                pConsole->gotoLigCol(i,j);
        }
    }
    ///direction vers le bas
    if ( dir == 's' )
    {
        if( i != MAXLIG)
        {
            i+= YINCR;
            pConsole->gotoLigCol(i,j);
        }else {
                i = MINLIG;
                pConsole->gotoLigCol(i,j);
        }
    }
    ///direction vers la droite
    if ( dir == 'd' )
    {
        if( j != MAXCOL )
        {
            j+= XINCR;
            pConsole->gotoLigCol(i,j);
        }else {
                j = MINCOL;
                pConsole->gotoLigCol(i,j);
        }
    }
    ///appui sur la touche entr� pour placer un pion
    if(dir == 13)
    {
        int y = (i - MINLIG)/YINCR;
        int x = (j - MINCOL)/XINCR;

        b.poserPion(y,x,play);
        //play = !play;
        b.drawBoard(play);
    }
    if(dir == 27)
    {
        /// touche echape
        //on sort du jeu
        SaveGame(b,play);
        ///lancer demande de sauvegarde

        return true;

    }
    return false;
Console::deleteInstance();

}

void score(bool play,Node b)
{
    //declaration des variables
    string player;
    pair <int,int> scoreJ12;


    // pointeur pour utiliser les fct de gotoligcol
    Console* pConsole = NULL;
    //allocation de memoire pour le ponteur
    pConsole = Console:: getInstance();

    scoreJ12 = b.CountTiles();

    pConsole->gotoLigCol(MINLIG,MAXCOL+5);

    //affihage du score de chaque joueur
    pConsole->setColor(COLOR_RED);
     cout<< "Joueur 1: " << scoreJ12.first;
     pConsole->setColor(COLOR_GREEN);
     cout<< "   Joueur 2: " << scoreJ12.second;

     pConsole->gotoLigCol(MINLIG+1,MAXCOL+5);
    if(play)
    {
        ///affichage du tour du joeur
        player = "Tour du J1";
       pConsole->setColor(COLOR_RED);

    }else {
        pConsole->setColor(COLOR_GREEN);
        player = "Tour du J2";
    }
    cout << player;
    pConsole->setColor(COLOR_WHITE);

    Console:: deleteInstance();
}
void displayMoves(std:: vector<int> moveLig,std:: vector <int> moveCol,int index)
{
    ///variables locales
    int lig = MINLIG+5;
    int col = MAXCOL+3;
    char charCol;
    // pointeur pour utiliser les fct de gotoligcol
    Console* pConsole = NULL;
    //allocation de memoire pour le ponteur
    pConsole = Console:: getInstance();

    pConsole->gotoLigCol(lig,col);
    cout<< "voici les coups possibles de l'adversaire:";
    lig++;
    for(unsigned int i = 0; i < moveCol.size(); i++)
    {
        ///si on tombe sur l'index choisit al�atoirement, alors on le met en vert
        if( i == abs(index))
        {
            pConsole->setColor(COLOR_GREEN);
        }else pConsole->setColor(COLOR_WHITE);

        pConsole->gotoLigCol(lig,col);

        /// traduction de la colone en lettre
        switch (moveCol[i])
        {
            case 0:
                charCol = 'A';
            break;
            case 1:
                charCol = 'B';
            break;
            case 2:
                charCol = 'C';
            break;
            case 3:
                charCol = 'D';
            break;
            case 4:
                charCol = 'E';
            break;
            case 5:
                charCol = 'F';
            break;
            case 6:
                charCol = 'G';
            break;
            case 7:
                charCol = 'H';
                break;
            default :
                charCol = 'X';

        }
        cout<< i+1 << ") " << moveLig[i] + 1 << " " << charCol << "         ";
        lig++;

    }
    ///efface les lignes qui pouraient etres apres
    for(int i =0;i<ERASELINES;i++)
    {
        cout << "                           ";
        lig++;
        pConsole->gotoLigCol(lig,col);
    }

}
