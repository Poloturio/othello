#include "Jeu.h"
#include "console.h"
#include "Fonction.h"
#include "Node.h"
#include "Pion.h"

#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>
#include <utility>
#include <fstream>


#define MAXLIG 17
#define MAXCOL 34
#define MINLIG 3
#define MINCOL 6
#define PLAY1 0
#define PLAY2 1
#define NBCELLS 8


using namespace std;

void Menu()
{
    int saisie = 0;
    bool correct = false;
    bool play = true;

    Node Board = Node();
    // pointeur pour utiliser les fct de gotoligcol
   // Console* pConsole = NULL;
    //allocation de memoire pour le ponteur
    //pConsole = Console:: getInstance();
    while (!correct)
    {
        system("CLS");
        cout<<" ________    ________    _      _    ________    _           _           ________  " << endl;
        cout<<"|   __   |  |        |  | |    | |  |   _____|  | |         | |         |   __   | " << endl;
        cout<<"|  |  |  |  |________|  | |    | |  |  |        | |         | |         |  |  |  | " <<endl;
        cout<<"|  |  |  |     |  |     | |____| |  |  |____    | |         | |         |  |  |  | "<<endl;
        cout<<"|  |  |  |     |  |     | |____| |  |  |____|   | |         | |         |  |  |  | "<<endl;
        cout<<"|  |__|  |     |  |     | |    | |  |  |_____   | |______   | |______   |  |__|  | "<<endl;
        cout<<"|________|     |__|     |_|    |_|  |________|  |________|  |________|  |________| "<<endl;
        cout<<" "<<endl;
        cout<<" "<<endl;
        cout<< "Bienvenue au jeu Othello, Veuillez choisir une option: " << endl;
        cout << "1) Lancer une partie" << endl;
        cout << "2) Charger une partie" << endl;
        cout << "3) Quitter le jeu" << endl;

        cin >> saisie;

        switch (saisie)
        {
            ///
            case 1:
                system("CLS");
                Board.boardInit();
                Board.setPlayer(play);
                Game(play,Board);
                correct = true;
                break;
            case 2:
               Board = loadGame(play);
               Game(play,Board);
                correct = true;
                break;
            case 3:
                correct = true;
                break;
            default:
                break;

        }

    }

}

void Game(bool play, Node Board)
{
    int playStyle = 0;
    bool correct = false;
    bool IA = false;
    int depth = 0;

    while (!correct)
    {
        system("CLS");

        cout << "Veuillez choisir un niveau de difficulte: " << endl;
        cout << "1) Mode 2 joueurs." << endl;
        cout << "2) Mode 1 joueur contre IA simple (pas de strategie)." << endl;
        cout << "3) Mode 1 joueur contre IA plus forte (Minimax profondeur: 5)" << endl;

        cin >> playStyle;

        switch (playStyle)
        {
            ///
            case 1:
                system("CLS");
                correct = true;
                break;
            case 2:
                system("CLS");
                IA = true;
                correct = true;
                break;
            case 3:
                system("CLS");
                IA = true;
                depth = 5;
                correct = true;
                break;
            default:
                break;

        }
    }
    ///initialisation du jeu
    bool sortie = false;

    int x = MINCOL;
    int y = MINLIG;
    /// premier joueur est le noir (rouge)




    /// affichage de la matrice
    tabprint();
    Board.drawBoard(play);
    score(play,Board);

///boucle de jeu

    while(!sortie)
    {

        /// g�re le d�placement du curseur sur le tableau 8X8
        if( (IA && play) || !IA )
        {
            ///
            //cout<< "saisie";
            sortie = saisieDir(y,x,Board,play);
           /// cout << x << " " << y;

        }else if(!play && depth == 0)
                {
                    ///tour de l'ordinateur
                    Board.AIMove(play);

                }else {
                //cout <<"mini";
                 int counter = 0;
                 Board.MiniMax(depth,play,counter,true,NBCELLS,MAXCOL+NBCELLS);
                 int i = Board.getMove(1);
                 int j = Board.getMove(2);
                 Board.poserPion(i,j,play);

                 Board.drawBoard(play);

                }

          if(EndGame(Board,play))
          {
              sortie = true;
          }



    }

}
///
//verifie les diff�rents cas de fin de jeu
bool EndGame(Node b, bool &play)
{
    ///on doit verifier le nombres de coup pour chauqe joueur
    if(b.checkEnd(play))
    {
        return false;

    }else if(b.checkEnd(!play))
        {
            play = !play;
            return false;

        }else{
                system("CLS");
                std:: pair <int,int> ScoreJ12 = b.CountTiles();
                if(ScoreJ12.first != ScoreJ12.second)
                {
                    if(ScoreJ12.first > ScoreJ12.second)
                    {
                        ///J1 a gagn�
                        cout << "Joueur 1 a gagne avec un score de: " << ScoreJ12.first << endl;
                        cout << "Le joueur 2 a perdu avec: " << ScoreJ12.second << endl;

                    }else{
                    ///J2 a gagn�
                    cout << "Joueur 2 a gagne! Avec un score de: " << ScoreJ12.second << endl;
                    cout << "Le joueur 1 a perdu avec: " << ScoreJ12.first << endl;
                    }
                }else{
                ///il y a eu �galit� entre les deux joueurs
                   cout << "Il y a egalite entre les deux joueurs! score, J1: " << ScoreJ12.first << " J2: " << ScoreJ12.second;
                }
                return true;
             }


}

void SaveGame(Node b, bool play)
{
    ///declaration de variables locales
    char validation;
    bool correct = false;

    ///blindage de saisie
    while (!correct)
    {
        system("CLS");
        //Efface ce qui est affich�
        cout << " Voulez vous sauvegarder? Entrez 'Y' pour oui et 'N' pour non. " << endl;
        cin >> validation;
        if (validation == 'Y' || validation == 'N')
        {
            correct = true;
        }
    }

    if(validation== 'Y')
    {
        std::string fileName("save.txt");
        //bool fin(false);
        ofstream fichier("save.txt", ios::out | ios::trunc);
         //Ouverture du fichier en �criture et efface le contenu du fichier
        for (int i=0;i<NBCELLS;i++)
        {
            for (int j=0;j<NBCELLS;j++)
            {
                fichier<<b.checkState(i,j);
                //Enregistrement dans un fichier des statuts de ttes les cases
            }
        }
        //On enregistre �galement quel joueur joue au prochain tour
        if (play==true)
        {
            fichier<<1;
        }else fichier<<2;

        fichier.close();
        //Fermeture du fichier
        cout << "Partie sauvegardee" << endl;
    }
    else
    {
        //Il annule donc on ne fait rien
    }
}

Node loadGame(bool &play)
{
    ifstream fichier("save.txt", ios::in);
    //Ouverture du fichier en lecture
    std:: vector<std:: vector<Pion>> b;
    b.resize( NBCELLS ,std:: vector<Pion>( NBCELLS , Pion() ) );
        if(fichier)
            // si l'ouverture a r�ussi
        {
            string partie;
            getline(fichier, partie);
        // on lit et prend la ligne �crite dans le fichier et on le met dans le string "partie"
            fichier.close();
            // on ferme le fichier
            for(int i=0;i<NBCELLS;i++)
            {
                for(int j=0;j<NBCELLS;j++)
                {
                    if(partie[(i*NBCELLS+j)] == '2')
                    {
                        //cout << "1";
                        b[i][j]=Pion(i,j,PLAY1);
                        //Il y a un pion du joueur 1 � cet emplacement
                    }
                    if(partie[(i*NBCELLS+j)] == '1')
                    {
                        //cout<< "2";
                        b[i][j]=Pion(i,j,PLAY2);
                        //Il y a un pion du joueur 2 � cet emplacement
                    }
                }
            }
            //cin >> partie;
            if(partie[65]==1)
            {
                play=true;
            }
            if(partie[65]==2)
            {
                play=false;
            }

            return Node(b);
        }
        else
            // sinon
                cerr << "Impossible d'ouvrir le fichier !" << endl;
    return Node();
}
